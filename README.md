Jasper Admin
============

Simple web interface for Jasper blockchain based on Multichain, written in PHP.

https://gitlab.com/jasperchain/JasperAdmin.git

    Copyright(C) Jasper Blockchain Technologies SAS (2019).
    Copyright(C) Coin Sciences Ltd (xxxx-2019).
    License: GNU Affero General Public License, see the file LICENSE.txt.


Welcome 
=======

This app currently supports the following features:

* Viewing the node's overall status.
* Creating addresses and giving them real names (names are visible to all nodes).
* Changing global permissions for addresses.
* Issuing assets, including custom fields and uploading a file.
* Updating assets, including issuing more units and updating custom fields and file.
* Viewing issued assets, including the full history of fields and files.
* Sending assets from one address to another.
* Creating, decoding and accepting offers for exchanges of assets.
* Creating streams.
* Publishing items to streams, as JSON or text or an uploaded file.
* Viewing stream items, including listing by key or publisher and downloading files.
* Writing, testing and approving Smart Filters (both transaction and stream filters).

The web demo does not yet support the following important functionality in the MultiChain API:

* Managing per-asset and per-stream permissions.
* Multisignature addresses and transactions.
* Adding metadata (or stream items) to permissions or asset transactions.
* Viewing an addresses' transactions.
* Subscribing to assets and viewing their transactions.
* Viewing a list of keys or publishers in a stream.
* Peer-to-peer node management.
* Message signing and verification.
* Blockchain upgrading.
* Working with the binary cache.

The Jasper Admin is still under development, so please [contact us](https://www.jasperfoundation.org/#contact) if any of these things are crucial for your needs.


System Requirements
-------------------

* A computer running web server software such as Apache.
* MultiChain 1.0 alpha 26 or later, including MultiChain 2.0 alphas and betas.
* Jasper Mainnet (for more info go to https://jaspercoin.io/qtoa)


Launch a Jasper MultiChain Blockchain
-----------------------------------------

    Download MultiChain
    Run : 
    multichaind jasper@jasperchain.tarpat.com:15444 -daemon 
    
If your web server is running on the same computer as `multichaind`, you can skip the rest of this section. Otherwise:

    multichain-cli jasper stop

Then add this to `~/.multichain/jasper/multichain.conf`:

    rpcallowip=[IP address of your web server]
  
Then start MultiChain again:
  
    multichaind jasper@jasperchain.tarpat.com:15444 -daemon 



Configure the Admin
----------------------

_This section assumes your blockchain is named `jasper` and you are running the node and web server on a Unix variant such as Linux. If not, please substitute accordingly._

Make your life easy for the next step by running these on the node's server:

    cat ~/.multichain/jasper/multichain.conf
    grep rpc-port ~/.multichain/jasper/params.dat
    
In the web demo directory, copy the `config-example.txt` file to `config.txt`:

	cp config-example.txt config.txt
  
In the demo website directory, enter chain details in `config.txt` e.g.:

    default.name=Default                # name to display in the web interface
    default.rpchost=127.0.0.1           # IP address of MultiChain node
    default.rpcport=12345               # usually default-rpc-port from params.dat
    default.rpcuser=multichainrpc       # username for RPC from multichain.conf
    default.rpcpassword=mnBh8aHp4mun... # password for RPC from multichain.conf


**Note that the `config.txt` file is readable by users of your installation, and contains your MultiChain API password, so you should never use this basic setup for a production system.**


Launch Jasper Admin
-------------------

No additional configuration or setup is required. Based on where you installed the web demo, open the appropriate address in your web browser, and you are ready to go!
